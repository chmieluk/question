package pl.com.britenet.questions.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.britenet.questions.domain.Answer;
import pl.com.britenet.questions.domain.dto.AnswerDto;
import pl.com.britenet.questions.services.AnswerService;

import java.util.List;

@RestController
@RequestMapping("/answers")
public class AnswerController {
    private final AnswerService answerService;
    @Autowired
    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @GetMapping("/")
    public ResponseEntity<List<AnswerDto>> getAll(){
        List<AnswerDto> answerDtoList = answerService.getAll();
        if(answerDtoList.isEmpty()){
            return new ResponseEntity<>(null,new HttpHeaders(), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(answerDtoList,new HttpHeaders(), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<AnswerDto> getAnswer(@PathVariable("id") Long id){
        AnswerDto answerDto = answerService.getAnswer(id);
        if(answerDto != null){
            return new ResponseEntity<>(answerDto,new HttpHeaders(),HttpStatus.OK);
        } else{
            return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.NOT_FOUND);
        }

    }
    @PostMapping("/add")
    public ResponseEntity<AnswerDto> add(@RequestBody AnswerDto answerDto){
        return  new ResponseEntity<>(answerService.save(answerDto),new HttpHeaders(),HttpStatus.CREATED);
    }
    @PutMapping("/edit/{id}")
    public ResponseEntity<AnswerDto> edit(@RequestBody AnswerDto answer,@PathVariable("id") Long id){
        if(answerService.ifExist(id)){
            if(id.equals(answer.getId())){
                AnswerDto saveAnswerDto = answerService.save(answer);
                return new ResponseEntity<>(saveAnswerDto,new HttpHeaders(),HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.BAD_REQUEST);
            }
        } else{
            return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.NOT_FOUND);
        }

    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id){
        if(answerService.ifExist(id)){
            answerService.delete(id);
            return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.OK);
        }
        return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.NOT_FOUND);
    }
}