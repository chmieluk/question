package pl.com.britenet.questions.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.britenet.questions.domain.dto.CategoryDto;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    @GetMapping("/")
    public ResponseEntity<List<CategoryDto>> getAll(){
        return null;
    }
    @GetMapping("/{id}")
    public ResponseEntity<CategoryDto> getCategory(@PathVariable("id") Long id){
        return null;
    }
    @PostMapping("/add/")
    public ResponseEntity<CategoryDto> add(@RequestBody CategoryDto categoryDto){
        return null;
    }
    @PutMapping("/edit/{id}")
    public ResponseEntity<CategoryDto> edit(@RequestBody CategoryDto categoryDto,@PathVariable("id") Long id){
        return null;
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id){
        return null;
    }
}
