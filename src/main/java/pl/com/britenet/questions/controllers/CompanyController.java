package pl.com.britenet.questions.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.britenet.questions.domain.Company;
import pl.com.britenet.questions.domain.dto.CompanyDto;
import pl.com.britenet.questions.services.CompanyService;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private CompanyService companyService;
    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/")
    public ResponseEntity<List<CompanyDto>> getAll(){
        List<CompanyDto> companyDtoList = companyService.getAll();
        if(companyDtoList.isEmpty()){
            return new ResponseEntity<>(null,new HttpHeaders(), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(companyDtoList,new HttpHeaders(), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<CompanyDto> getCompany(@PathVariable("id") Long id){
        if(companyService.ifExist(id)){
            return new ResponseEntity<>(companyService.getCompany(id),new HttpHeaders(),HttpStatus.OK);
        }
        return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.NOT_FOUND);
    }
    @PostMapping("/add/")
    public ResponseEntity<CompanyDto> add(@RequestBody CompanyDto companyDto){
        return new ResponseEntity<>(companyService.save(companyDto),new HttpHeaders(),HttpStatus.CREATED);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<CompanyDto> edit(@RequestBody CompanyDto companyDto,@PathVariable("id") Long id){
        if(companyService.ifExist(id)){
            if(id.equals(companyDto.getId())){
                return new ResponseEntity<>(companyService.save(companyDto),new HttpHeaders(),HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.NOT_FOUND);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id){
        if(companyService.ifExist(id)){
            companyService.delete(id);
            return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.OK);
        }
        return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.NOT_FOUND);
    }
}
