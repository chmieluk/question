package pl.com.britenet.questions.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.britenet.questions.domain.dto.QuestionDto;
import pl.com.britenet.questions.services.QuestionService;

import javax.validation.Valid;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private final QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/")
    public ResponseEntity<Page<QuestionDto>> getAllQuestions(Pageable pageable) {
        Page<QuestionDto> questionDtoPage = questionService.findAllByPage(pageable);

        if (!questionDtoPage.getContent().isEmpty()) {
            return new ResponseEntity<>(questionDtoPage, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/search/{question}")
    public ResponseEntity<Page<QuestionDto>> searchQuestion(Pageable pageable,
                                                            @PathVariable("question") String question) {
        Page<QuestionDto> questionDtoPage = questionService.findAllByPage(question, pageable);
        if (!questionDtoPage.getContent().isEmpty()) {
            return new ResponseEntity<>(questionDtoPage, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<QuestionDto> getQuestion(@PathVariable("id") Long id) {
        QuestionDto questionDto = questionService.getQuestion(id);
        if (questionDto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(questionDto, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/add/")
    public ResponseEntity<QuestionDto> add(@Valid @RequestBody QuestionDto question) {
        QuestionDto saveQuestionDto = questionService.save(question);
        return new ResponseEntity<>(saveQuestionDto, new HttpHeaders(), HttpStatus.CREATED);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<QuestionDto> edit(@RequestBody QuestionDto question, @PathVariable("id") Long id) {
        if (questionService.ifExist(id)) {
            if (question.getId().equals(id)) {
                QuestionDto saveQuestionDto = questionService.save(question);
                return new ResponseEntity<>(saveQuestionDto, new HttpHeaders(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        if (questionService.ifExist(id)) {
            questionService.delete(id);
            return new ResponseEntity(null, new HttpHeaders(), HttpStatus.OK);
        }
        return new ResponseEntity(null, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }
}
