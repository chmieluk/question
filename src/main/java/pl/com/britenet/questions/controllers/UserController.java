package pl.com.britenet.questions.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.britenet.questions.domain.User;
import pl.com.britenet.questions.domain.dto.UserDto;
import pl.com.britenet.questions.services.UserService;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }
    @GetMapping("/")
    public ResponseEntity<List<UserDto>> getAll(){
        List<UserDto> userDtoList = userService.getAll();
        if(userDtoList.isEmpty()){
            return new ResponseEntity<>(null,new HttpHeaders(), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(userDtoList,new HttpHeaders(), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable("id") Long id){
        if(userService.ifExist(id)){
            return new ResponseEntity<>(userService.getUser(id),new HttpHeaders(),HttpStatus.OK);
        }
        return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.NOT_FOUND);
    }
    @PostMapping("/add/")
    public ResponseEntity<UserDto> add(@RequestBody UserDto userDto){
        return new ResponseEntity<>(userService.save(userDto),new HttpHeaders(),HttpStatus.CREATED);
    }
    @PutMapping("/edit/{id}")
    public ResponseEntity<UserDto> edit(@RequestBody UserDto userDto,@PathVariable("id") Long id){
        if(userService.ifExist(id)){
            if(id.equals(userDto.getId())){
                return new ResponseEntity<>(userService.save(userDto),new HttpHeaders(),HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.NOT_FOUND);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id){
        if(userService.ifExist(id)){
            userService.delete(id);
            return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.OK);
        }
        return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.NOT_FOUND);
    }
}
