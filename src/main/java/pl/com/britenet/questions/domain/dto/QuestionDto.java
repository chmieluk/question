package pl.com.britenet.questions.domain.dto;

import org.springframework.format.annotation.DateTimeFormat;
import pl.com.britenet.questions.domain.Category;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class QuestionDto {
    private Long id;
    @Size(min = 10, max = 255)
    private String question;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date createDate;
    private UserDto users;
    private CompanyDto company;
    private List<AnswerDto> answers;
    private Set<CategoryDto> categories;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public UserDto getUsers() {
        return users;
    }

    public void setUsers(UserDto users) {
        this.users = users;
    }

    public CompanyDto getCompany() {
        return company;
    }

    public void setCompany(CompanyDto company) {
        this.company = company;
    }

    public List<AnswerDto> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDto> answers) {
        this.answers = answers;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Set<CategoryDto> getCategories() {
        return categories;
    }

    public void setCategories(Set<CategoryDto> categories) {
        this.categories = categories;
    }
}
