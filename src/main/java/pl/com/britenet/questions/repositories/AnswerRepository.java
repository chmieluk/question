package pl.com.britenet.questions.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.com.britenet.questions.domain.Answer;
@Repository
public interface AnswerRepository extends CrudRepository<Answer,Long> {
}
