package pl.com.britenet.questions.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.com.britenet.questions.domain.Question;

@Repository
public interface QuestionRepository extends CrudRepository<Question,Long> {
    Page<Question> findAll(Pageable pageable);
    Page<Question> findAllByQuestionIgnoreCaseContaining(String question, Pageable pageable);
}
