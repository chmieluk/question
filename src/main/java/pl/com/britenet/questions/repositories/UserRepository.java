package pl.com.britenet.questions.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.com.britenet.questions.domain.User;

@Repository
public interface UserRepository extends CrudRepository<User,Long> {
    @Query("select u from pl.com.britenet.questions.domain.User u where u.id=:id")
    User getMyUserById(@Param("id") Long id);
}
