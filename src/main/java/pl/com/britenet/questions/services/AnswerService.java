package pl.com.britenet.questions.services;

import pl.com.britenet.questions.domain.Answer;
import pl.com.britenet.questions.domain.dto.AnswerDto;

import java.util.List;

public interface AnswerService {
    AnswerDto getAnswer(Long id);
    List<AnswerDto> getAll();
    AnswerDto save(AnswerDto answerDto);
    void delete(Long id);
    boolean ifExist(Long id);
}
