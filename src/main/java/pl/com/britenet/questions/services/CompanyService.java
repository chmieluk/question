package pl.com.britenet.questions.services;

import pl.com.britenet.questions.domain.Company;
import pl.com.britenet.questions.domain.dto.CompanyDto;

import java.util.List;

public interface CompanyService {
    List<CompanyDto> getAll();
    CompanyDto getCompany(Long id);
    CompanyDto save(CompanyDto companyDto);
    void delete(Long id);
    boolean ifExist(Long id);
}
