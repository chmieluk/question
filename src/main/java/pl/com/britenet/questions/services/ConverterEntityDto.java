package pl.com.britenet.questions.services;

import org.hibernate.Hibernate;
import pl.com.britenet.questions.domain.Answer;
import pl.com.britenet.questions.domain.Question;

import java.util.List;
import java.util.stream.Collectors;


public interface ConverterEntityDto {
    Object convertToDto(Object entity);

    Object convertToEntity(Object dto);
    default List<Question> clearQuestionsFromRelation(List<Question> questionList) {
        if (questionList != null && !questionList.isEmpty()) {
            questionList = questionList.stream().map(question -> {
                question.setAnswers(null);
                question.setCompany(null);
                question.setUsers(null);
                return question;
            }).collect(Collectors.toList());
        }
        return questionList;

    }

    default List<Answer> clearAnswersFromRelation(List<Answer> answerList) {
        if (answerList != null && !answerList.isEmpty()) {
            answerList = answerList.stream().map(answer -> {
                answer.setQuestion(null);
                answer.setUsers(null);
                return answer;
            }).collect(Collectors.toList());
        }
        return answerList;

    }
}
