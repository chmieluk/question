package pl.com.britenet.questions.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.com.britenet.questions.domain.Question;
import pl.com.britenet.questions.domain.dto.QuestionDto;

import java.util.List;

public interface QuestionService {
    List<Question> getAll();
    QuestionDto getQuestion(Long id);
    QuestionDto save(QuestionDto question);
    //do zastanowienia które użyć
    void delete(Question question);
    void delete(Long id);
    boolean ifExist(Long id);
    Page<QuestionDto> findAllByPage(Pageable pagin);
    Page<QuestionDto> findAllByPage(String questionName, Pageable pagin);
}
