package pl.com.britenet.questions.services;

import pl.com.britenet.questions.domain.User;
import pl.com.britenet.questions.domain.dto.UserDto;

import java.util.List;

public interface UserService {
    List<UserDto> getAll();
    UserDto getUser(Long id);
    UserDto save(UserDto user);
    void delete(Long id);
    boolean ifExist(Long id);
}
