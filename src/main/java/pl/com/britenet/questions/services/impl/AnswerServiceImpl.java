package pl.com.britenet.questions.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.com.britenet.questions.domain.Answer;
import pl.com.britenet.questions.domain.Question;
import pl.com.britenet.questions.domain.dto.AnswerDto;
import pl.com.britenet.questions.repositories.AnswerRepository;
import pl.com.britenet.questions.services.AnswerService;
import pl.com.britenet.questions.services.ConverterEntityDto;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AnswerServiceImpl implements AnswerService{
    private final AnswerRepository answerRepository;
    private final ConverterEntityDto converterEntityDto;
    @Autowired
    public AnswerServiceImpl(AnswerRepository answerRepository,
                             @Qualifier("converterAnswerEntityDtoImpl") ConverterEntityDto converterEntityDto) {
        this.answerRepository = answerRepository;
        this.converterEntityDto = converterEntityDto;
    }

    @Override
    public AnswerDto getAnswer(Long id) {
        return (AnswerDto) converterEntityDto.convertToDto(answerRepository.findOne(id));
    }

    @Override
    public List<AnswerDto> getAll() {
        return ((List<Answer>) answerRepository.findAll()).stream()
                .map(answerDto -> {return (AnswerDto) converterEntityDto.convertToDto(answerDto);})
                .collect(Collectors.toList());
    }

    @Override
    public AnswerDto save(AnswerDto answer) {
        return (AnswerDto) converterEntityDto.convertToDto(answerRepository.save((Answer) converterEntityDto.convertToEntity(answer)));
    }

    @Override
    public void delete(Long id) {
        answerRepository.delete(id);
    }

    @Override
    public boolean ifExist(Long id) {
        return answerRepository.exists(id);
    }


}
