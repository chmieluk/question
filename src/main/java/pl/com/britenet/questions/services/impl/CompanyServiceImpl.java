package pl.com.britenet.questions.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.com.britenet.questions.domain.Company;
import pl.com.britenet.questions.domain.Question;
import pl.com.britenet.questions.domain.dto.CompanyDto;
import pl.com.britenet.questions.repositories.CompanyRepository;
import pl.com.britenet.questions.services.CompanyService;
import pl.com.britenet.questions.services.ConverterEntityDto;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService {
    private final CompanyRepository companyRepository;
    private final ConverterEntityDto converterEntityDto;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository,
                              @Qualifier("converterCompanyEntityDtoImpl") ConverterEntityDto converterEntityDto) {
        this.companyRepository = companyRepository;
        this.converterEntityDto = converterEntityDto;
    }




    @Override
    public List<CompanyDto> getAll() {

        List<Company> companyList =  (List<Company>) companyRepository.findAll();
        List<CompanyDto> companyDtoList = companyList.stream()
                .map(company -> {return (CompanyDto)converterEntityDto.convertToDto(company);})
                .collect(Collectors.toList());
        return companyDtoList;
    }

    @Override
    public CompanyDto getCompany(Long id) {
        return (CompanyDto) converterEntityDto.convertToDto(companyRepository.findOne(id));
    }

    @Override
    public CompanyDto save(CompanyDto company) {
        return (CompanyDto) converterEntityDto.convertToDto(companyRepository.save((Company)converterEntityDto.convertToEntity(company)));
    }

    @Override
    public void delete(Long id) {
        companyRepository.delete(id);
    }

    @Override
    public boolean ifExist(Long id) {
        return companyRepository.exists(id);
    }


}
