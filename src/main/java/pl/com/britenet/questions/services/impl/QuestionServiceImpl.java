package pl.com.britenet.questions.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.com.britenet.questions.domain.Question;
import pl.com.britenet.questions.domain.dto.QuestionDto;
import pl.com.britenet.questions.repositories.QuestionRepository;
import pl.com.britenet.questions.services.ConverterEntityDto;
import pl.com.britenet.questions.services.QuestionService;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final ConverterEntityDto converterEntityDto;
    @Autowired
    public QuestionServiceImpl(QuestionRepository questionRepository,
                               @Qualifier("converterQuestionEntityDtoImpl") ConverterEntityDto converterEntityDto) {
        this.questionRepository = questionRepository;
        this.converterEntityDto = converterEntityDto;
    }

    @Override
    public List<Question> getAll() {
        return (List<Question>) questionRepository.findAll();
    }

    @Override
    public QuestionDto getQuestion(Long id) {
        return (QuestionDto) converterEntityDto.convertToDto(questionRepository.findOne(id));
    }

    @Override
    public QuestionDto save(QuestionDto question) {
        return (QuestionDto) converterEntityDto.convertToDto(questionRepository.save((Question) converterEntityDto.convertToEntity(question)));
    }

    @Override
    public void delete(Question question) {

    }

    @Override
    public void delete(Long id) {
        questionRepository.delete(id);
    }

    @Override
    public boolean ifExist(Long id) {
        return questionRepository.exists(id);
    }

    @Override
    public Page<QuestionDto> findAllByPage(Pageable pagin) {
        Page<Question> questions = questionRepository.findAll(pagin);
        return questions.map(this::convertToDto);
    }

    @Override
    public Page<QuestionDto> findAllByPage(String questionName,Pageable pagin) {
        Page<Question> questions = questionRepository.findAllByQuestionIgnoreCaseContaining(questionName,pagin);
        return questions.map(this::convertToDto);
    }
    private QuestionDto convertToDto(Question question) {
        return (QuestionDto) converterEntityDto.convertToDto(question);
    }
}
