package pl.com.britenet.questions.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.com.britenet.questions.domain.User;
import pl.com.britenet.questions.domain.dto.UserDto;
import pl.com.britenet.questions.repositories.UserRepository;
import pl.com.britenet.questions.services.ConverterEntityDto;
import pl.com.britenet.questions.services.UserService;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ConverterEntityDto converterEntityDto;


    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           @Qualifier("converterUserEntityDtoImpl") ConverterEntityDto converterEntityDto) {
        this.userRepository = userRepository;
        this.converterEntityDto = converterEntityDto;
    }


    @Override
    public List<UserDto> getAll() {
        List<User> userList = (List<User>) userRepository.findAll();
        return userList.stream()
                .map(user -> {return (UserDto) converterEntityDto.convertToDto(user);})
                .collect(Collectors.toList());
    }

    @Override
    public UserDto getUser(Long id) {
        return (UserDto) converterEntityDto.convertToDto(userRepository.findOne(id));
    }

    @Override
    public UserDto save(UserDto user) {
        return (UserDto) converterEntityDto.convertToDto(userRepository.save((User) converterEntityDto.convertToEntity(user)));
    }

    @Override
    public void delete(Long id) {
        userRepository.delete(id);
    }
    @Override
    public boolean ifExist(Long id) {
        return userRepository.exists(id);
    }

}
