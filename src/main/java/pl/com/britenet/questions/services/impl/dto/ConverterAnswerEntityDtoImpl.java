package pl.com.britenet.questions.services.impl.dto;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.britenet.questions.domain.Answer;
import pl.com.britenet.questions.domain.Question;
import pl.com.britenet.questions.domain.User;
import pl.com.britenet.questions.domain.dto.AnswerDto;
import pl.com.britenet.questions.services.ConverterEntityDto;

@Service
public class ConverterAnswerEntityDtoImpl implements ConverterEntityDto {

    private final ModelMapper modelMapper;

    @Autowired
    public ConverterAnswerEntityDtoImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public AnswerDto convertToDto(Object answer) {
        Question question = ((Answer)answer).getQuestion();
        question.setUsers(null);
        question.setCompany(null);
        question.setAnswers(null);
        ((Answer) answer).setQuestion(question);
        User users = ((Answer) answer).getUsers();
        users.setAnswers(null);
        users.setQuestions(null);
        ((Answer) answer).setUsers(users);
        return modelMapper.map(answer,AnswerDto.class);
    }

    @Override
    public Answer convertToEntity(Object answerDto) {
        return modelMapper.map(answerDto,Answer.class);
    }

}
