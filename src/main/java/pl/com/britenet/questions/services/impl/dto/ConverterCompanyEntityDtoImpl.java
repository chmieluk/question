package pl.com.britenet.questions.services.impl.dto;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.britenet.questions.domain.Company;
import pl.com.britenet.questions.domain.Question;
import pl.com.britenet.questions.domain.dto.CompanyDto;
import pl.com.britenet.questions.services.ConverterEntityDto;

import java.util.List;

@Service
class ConverterCompanyEntityDtoImpl implements ConverterEntityDto {
    private final ModelMapper modelMapper;

    @Autowired
    public ConverterCompanyEntityDtoImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public CompanyDto convertToDto(Object company) {
        List<Question> questionList = ((Company) company).getQuestions();
        questionList = clearQuestionsFromRelation(questionList);
        ((Company) company).setQuestions(questionList);
        CompanyDto companyDto = modelMapper.map(company, CompanyDto.class);
        return companyDto;
    }

    @Override
    public Company convertToEntity(Object companyDto) {
        return modelMapper.map(companyDto,Company.class);
    }

}
