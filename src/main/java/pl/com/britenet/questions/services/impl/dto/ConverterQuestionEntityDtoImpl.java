package pl.com.britenet.questions.services.impl.dto;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.britenet.questions.domain.Answer;
import pl.com.britenet.questions.domain.Company;
import pl.com.britenet.questions.domain.Question;
import pl.com.britenet.questions.domain.User;
import pl.com.britenet.questions.domain.dto.QuestionDto;
import pl.com.britenet.questions.services.ConverterEntityDto;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConverterQuestionEntityDtoImpl implements ConverterEntityDto{
    private final ModelMapper modelMapper;

    @Autowired
    public ConverterQuestionEntityDtoImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public QuestionDto convertToDto(Object question) {
        User user = ((Question) question).getUsers();
        if(user != null){
            user.setQuestions(null);
            user.setAnswers(null);
            ((Question) question).setUsers(user);
        }
        Company company = ((Question) question).getCompany();
        if(company != null){
            company.setQuestions(null);
            ((Question) question).setCompany(company);
        }
        List<Answer> answers = ((Question) question).getAnswers();
        if(answers != null){
            List<Answer> answersWitoutQuestion = answers.stream().map(p -> {
                p.setQuestion(null);
                return p;
            }).collect(Collectors.toList());
            ((Question) question).setAnswers(answersWitoutQuestion);
        }
        return modelMapper.map(question,QuestionDto.class);

    }

    @Override
    public Question convertToEntity(Object questionDto) {
        return modelMapper.map(questionDto,Question.class);
    }
}
