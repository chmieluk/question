package pl.com.britenet.questions.services.impl.dto;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.britenet.questions.domain.Answer;
import pl.com.britenet.questions.domain.Question;
import pl.com.britenet.questions.domain.User;
import pl.com.britenet.questions.domain.dto.UserDto;
import pl.com.britenet.questions.services.ConverterEntityDto;

import java.util.List;

@Service
public class ConverterUserEntityDtoImpl implements ConverterEntityDto{

    private final ModelMapper modelMapper;

    @Autowired
    public ConverterUserEntityDtoImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public UserDto convertToDto(Object user) {
        List<Question> questionList = ((User) user).getQuestions();
        questionList = clearQuestionsFromRelation(questionList);
        ((User) user).setQuestions(questionList);
        List<Answer> answers = ((User) user).getAnswers();
        List<Answer> answerList = clearAnswersFromRelation(answers);
        ((User) user).setAnswers(answerList);
        return modelMapper.map(user,UserDto.class);
    }

    @Override
    public User convertToEntity(Object userDto) {
        return modelMapper.map(userDto,User.class);
    }

}
