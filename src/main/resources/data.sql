INSERT INTO public.users (active, login, name, surname) VALUES ( TRUE , 'jan.kowalski', 'Jan', 'Kowalski');
INSERT INTO public.users (active, login, name, surname) VALUES ( TRUE , 'piotr.nowak', 'Piotr', 'Nowak');

INSERT INTO public.company (name)  VALUES ('Britenet');
INSERT INTO public.company (name)  VALUES ('SII');

INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 1',1,1);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 2',2,2);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 3',1,1);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 4',2,2);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 5',1,1);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 6',2,2);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 7',1,1);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 8',2,2);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 9',1,1);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 10',2,2);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 11',1,1);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 12',2,2);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 13',1,1);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 14',2,2);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 15',1,1);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 16',2,2);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 17',1,1);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 18',2,2);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 19',1,1);
INSERT INTO public.question (question,company,users) VALUES ('Pytanie nr 20',2,2);


INSERT INTO public.answer (answer,question,users) VALUES ('Odpowiedź nr 1',1,1);
INSERT INTO public.answer (answer,question,users) VALUES ('Odpowiedź nr 2',1,1);
INSERT INTO public.answer (answer,question,users) VALUES ('Odpowiedź nr 3',2,2);

INSERT INTO public.category (name)  VALUES ('Java');
INSERT INTO public.category (name)  VALUES ('Spring');
INSERT INTO public.category (name)  VALUES ('Angular');

INSERT INTO public.question_category (question_id,category_id)  VALUES (1,1);
INSERT INTO public.question_category (question_id,category_id)  VALUES (1,2);
INSERT INTO public.question_category (question_id,category_id)  VALUES (3,3);
