package pl.com.britenet.questions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.britenet.questions.domain.User;
import pl.com.britenet.questions.repositories.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.math.BigInteger;

import static org.assertj.core.api.Assertions.assertThat;
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class UserServiceTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EntityManager entityManager;

    @Test
    public void findUser(){
        User userRepositoryOne = userRepository.findOne(1L);
        System.out.println(userRepositoryOne.getId());
    }
    @Test
    public void testNamedQuery(){
        Query namedQuery = entityManager.createNamedQuery("User.findActive");
        Object singleResult = namedQuery.setParameter("id", 1L).getSingleResult();

        Query nativeQuery = entityManager.createNamedQuery("user.findUserNativ");
        Object[] singleResult1 = (Object[])nativeQuery.setParameter("id", 1L).getSingleResult();
        Long id = ((BigInteger) singleResult1[0]).longValue();
        User user = userRepository.getMyUserById(1L);
        assertThat(id).isEqualTo(((User)singleResult).getId()).isEqualTo(user.getId());
        System.out.println(user.getLogin());
    }
}
