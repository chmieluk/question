package pl.com.britenet.questions;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import pl.com.britenet.questions.controllers.UserController;
import pl.com.britenet.questions.domain.User;
import pl.com.britenet.questions.domain.dto.UserDto;
import pl.com.britenet.questions.services.ConverterEntityDto;
import pl.com.britenet.questions.services.UserService;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@WebAppConfiguration
public class UserTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Before
    public void setUp() {
        UserDto userDto = new UserDto();
        userDto.setId(1L);
        userDto.setLogin("test");
        userDto.setName("Jan");
        userDto.setSurname("Kowalski");
        Mockito.when(userService.getUser(Matchers.anyLong())).thenReturn(userDto);
        Mockito.when(userService.ifExist(Matchers.anyLong())).thenReturn(true);
        Mockito.when(userService.ifExist(Matchers.eq(2L))).thenReturn(false);
    }

    @Test
    public void findUser() {
        try {
            mockMvc.perform(get("/users/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.login",is("test")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
